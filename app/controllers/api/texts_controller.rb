class API::TextsController < ApplicationController
  def show
    @texts = Text.first
    render json: @texts
  end
end
