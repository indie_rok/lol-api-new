class API::MatchesController < ApplicationController
  def upcoming
    @matches = Match.order(:time).where('time > ?', DateTime.now).select(:score_a,:score_b,:team_a_id,:team_b_id,:time).limit(6)

    render json: @matches, include: { teams: { only: [:name,:logo_url]} }, :callback  => params[:callback]
  end
end
