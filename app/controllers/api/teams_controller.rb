class API::TeamsController < ApplicationController
  def by_points
    @teams = Team.order(points: :desc).select(:name,:logo_url,:points)
    render json: @teams, :callback  => params[:callback]
  end

  def index
    @teams  = Team.select(:id,:name,:logo_url,:description).order("
    CASE
      WHEN name = 'Lyon Gaming' THEN '1'
      WHEN name = 'Just Toys Havoks' THEN '2'
      WHEN name = 'Galactic Gamers' THEN '3'
      WHEN name = 'Revenge eSports' THEN '4'
      WHEN name = 'Zaga Talent Gaming' THEN '5'
      WHEN name = 'Brawl eSports' THEN '6'
    ELSE
      '7'
    END")
    render json: @teams, :callback  => params[:callback], methods: :ordered_players
  end

  def by_group
    @groups = Group.all
    #render :json => @groups, :include => {:teams => {:only => []}}
    render json: @groups, methods: :ordered_teams, :except => [:id]
  end
end
