class API::OptionsController < ApplicationController
  def show
    @options  = Option.select(
                      :playing_live_video,
                      :logo_url,:live_video_url,
                      :facebook_url,
                      :twitter_url,
                      :instagram_url,
                      :youtube_url,
                      :mas_noticias_url,
                      :logo_url,
                      :tournament_name,
                      :group_table,
                      :current_journey

    ).first

    @options.current_journey = pretty_journeys(@options.current_journey)
    render json: @options, :callback  => params[:callback]
  end
end
