class API::LeaderboardsController < ApplicationController
  def show
    @teams = Team.order(points: :desc).select(:change_place_img,:name,:victories,:defeats,:kills,:deaths,:points,:logo_url)
    render json: @teams, :callback  => params[:callback]
  end
end
