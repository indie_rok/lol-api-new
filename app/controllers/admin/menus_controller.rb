class Admin::MenusController < ApplicationController
  before_filter :authorize

  # GET /admin/menus/1/edit
  def edit
    @menus = Menu.order(:id).all
  end


  # PATCH/PUT /admin/menus/
  def update

    admin_menu_params['menus'].each do |menu_item|
      menu = Menu.find(menu_item['id'].to_i)
      menu.title = menu_item['title']
      if  menu_item['active'] == nil
        menu.active = false
      elsif menu_item['active'] == "1"
        menu.active = true
      end
      menu.save!
    end

    flash[:success] = "Editados"

    redirect_to edit_admin_menu_path

  end


  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_menu_params
      params.permit(menus: [:id,:title,:active])
    end
end
