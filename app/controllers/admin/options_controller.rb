class Admin::OptionsController < ApplicationController
  before_filter :authorize
  def edit
    @options = Option.first
  end

  def update
    if Option.first.update option_params
      flash[:success] = "Editados"
    else
      flash[:danger] = "Errors"
    end

    redirect_to edit_admin_options_path
  end


  def option_params
    params.permit(:logo_url,
                  :tournament_name,
                  :playing_live_video,
                  :live_video_url,
                  :facebook_url,
                  :twitter_url,:instagram_url,
                  :youtube_url,
                  :mas_noticias_url,
                  :group_table,
                  :current_journey
    )
  end
end
