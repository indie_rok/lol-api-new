class SessionsController < ApplicationController
  def new
    @disable_nav = true
  end

  def create
    user = Option.find_by_username(params[:username])
    # If the user exists AND the password entered is correct.
    if user && user.authenticate(params[:password])
      # Save the user id inside the browser cookie. This is how we keep the user
      # logged in when they navigate around our website.
      session[:user_id] = user.id
      flash[:success]  = "Logeado!"
      redirect_to edit_admin_calendar_path
    else
    # If user's login doesn't work, send them back to the login form.
      flash[:danger]  = "Nombre de usuario o contraseña incorrecta"
      redirect_to new_session_path
    end

  end

  def destroy
    session[:user_id] = nil
    flash[:success] = "Bye bye"
    redirect_to new_session_path
  end


end
