class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token, if: :json_request?

  # THIS
  before_action :set_headers

  def current_user
    @current_user ||= Option.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def authorize
    unless current_user
      flash[:danger] = "Necesitas Logearte"
      redirect_to new_session_path
    end

  end

  def json_request?
    request.format.json?
  end

  # THIS
  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end


  def pretty_journeys string
    if string =~ /\d/
      string  = string.gsub(/[^0-9]/, '')
    end

    return string
  end

end
