class Option < ApplicationRecord

  has_secure_password
  
  validates :facebook_url, presence: true
  validates :twitter_url, presence: true
  validates :instagram_url, presence: true
  validates :youtube_url, presence: true
  validates :mas_noticias_url, presence: true
end
