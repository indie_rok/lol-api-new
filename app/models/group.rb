class Group < ApplicationRecord
  validates_presence_of :name
  validates_uniqueness_of :name, {case_sensitive: true}

  has_many :teams, dependent:  :nullify

  def ordered_teams
    self.teams.select(:name,:victories,:defeats,:points,:logo_url,:short_name).order(:points => :desc )
  end
end