class Article < ApplicationRecord
  acts_as_sortable
  validates :url, presence: true
  validates :title, presence: true
end
