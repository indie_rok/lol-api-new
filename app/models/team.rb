class Team < ApplicationRecord

  validates :name, presence: true
  validates :victories, numericality: { only_integer: true }
  validates :defeats, numericality: { only_integer: true }
  validates :kills, numericality: { only_integer: true }
  validates :deaths, numericality: { only_integer: true }
  validates :points, numericality: { only_integer: true }

  has_many :players, -> { where(active: true) }, dependent: :nullify
  belongs_to :group


  def matches
    Match.where("team_a_id = ? OR team_b_id = ?", self.id, self.id)
  end

  def ordered_players
    query = "SELECT * FROM Players WHERE team_id= #{self.id} AND active = true  ORDER BY CASE
                    WHEN position='Superior' THEN  1
                    WHEN position='Jungla' THEN 2
                    WHEN position='Central' THEN 3
                    WHEN position='Tirador' THEN 4
                    WHEN position='Soporte' THEN 5
                ELSE 6
              END";
    results = ActiveRecord::Base.connection.execute(query)
  end

end
