class Journey < ApplicationRecord

  @@counter = 0
  validates :name, presence: true

  has_many :matches, -> { order(:time) },dependent: :destroy

  def by_group
    self.matches
  end

  def test
    groups = []
    self.matches.each{|match|groups << match.a_team.group.name}
    groups.each {}
  end

  def counter_journey
    @@counter = @@counter +1
  end


end
