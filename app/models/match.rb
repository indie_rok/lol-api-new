class Match < ApplicationRecord

  validates :team_a_id, presence: true
  validates :team_b_id, presence: true

  belongs_to :a_team, class_name: 'Team', foreign_key: 'team_a_id'
  belongs_to :b_team, class_name: 'Team', foreign_key: 'team_b_id'
  belongs_to :journey

  def teams
    [a_team, b_team]
  end

end

