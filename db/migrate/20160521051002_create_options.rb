class CreateOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :options do |t|
      t.string :username
      t.string :password_digest
      t.boolean :playing_live_video, default: false
      t.string :live_video_url
      t.string :facebook_url
      t.string :twitter_url
      t.string :instagram_url
      t.string :youtube_url
      t.string :mas_noticias_url

    end
  end
end
