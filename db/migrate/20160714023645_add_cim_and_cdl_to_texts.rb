class AddCimAndCdlToTexts < ActiveRecord::Migration[5.0]
  def change
    add_column :texts, :cim, :string
    add_column :texts, :cdl, :string
  end
end
