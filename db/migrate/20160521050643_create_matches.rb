class CreateMatches < ActiveRecord::Migration[5.0]
  def change
    create_table :matches do |t|
      t.integer :team_a_id
      t.integer :team_b_id
      t.integer :score_a, default: 0
      t.integer :score_b, default: 0
      t.datetime :time
      t.references :journey, foreign_key: true

    end
  end
end
