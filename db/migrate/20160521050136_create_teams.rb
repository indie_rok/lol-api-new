class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :logo_url
      t.text :description
      t.integer :victories, default: 0
      t.integer :defeats, default: 0
      t.integer :kills, default: 0
      t.integer :deaths, default: 0
      t.integer :points, default: 0
      t.string :change_place_img, default:'none'

    end
  end
end
