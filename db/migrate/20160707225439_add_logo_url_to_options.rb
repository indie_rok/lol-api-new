class AddLogoUrlToOptions < ActiveRecord::Migration[5.0]
  def change
    add_column :options, :logo_url, :string
  end
end
