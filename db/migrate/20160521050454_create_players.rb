class CreatePlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :players do |t|
      t.string :name
      t.string :position
      t.string :logo_url
      t.boolean :active, default: true
      t.integer :team_id
      t.string :kda, default: '0/0/0'
    end
  end
end
