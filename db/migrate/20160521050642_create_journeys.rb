class CreateJourneys < ActiveRecord::Migration[5.0]
  def change
    create_table :journeys do |t|
      t.string :name
      t.boolean :need_brackets, default:false
      t.string :brackets_url
      t.date :date

    end
  end
end
