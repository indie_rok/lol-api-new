class AddGroupTableToOptions < ActiveRecord::Migration[5.0]
  def change
    add_column :options, :group_table, :boolean
  end
end
