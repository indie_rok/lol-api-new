class AddCurrentJourneyToOptions < ActiveRecord::Migration[5.0]
  def change
    add_column :options, :current_journey, :string
  end
end
