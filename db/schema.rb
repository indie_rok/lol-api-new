# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160714161636) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.string  "url"
    t.boolean "is_youtube", default: false
    t.string  "thumb_url"
    t.string  "title"
    t.string  "subtitle"
    t.date    "date"
    t.integer "position",                   null: false
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
  end

  create_table "journeys", force: :cascade do |t|
    t.string  "name"
    t.boolean "need_brackets", default: false
    t.string  "brackets_url"
    t.date    "date"
  end

  create_table "matches", force: :cascade do |t|
    t.integer  "team_a_id"
    t.integer  "team_b_id"
    t.integer  "score_a",    default: 0
    t.integer  "score_b",    default: 0
    t.datetime "time"
    t.integer  "journey_id"
    t.index ["journey_id"], name: "index_matches_on_journey_id", using: :btree
  end

  create_table "menus", force: :cascade do |t|
    t.string   "title"
    t.boolean  "active"
    t.integer  "parent_menu_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "options", force: :cascade do |t|
    t.string  "username"
    t.string  "password_digest"
    t.boolean "playing_live_video", default: false
    t.string  "live_video_url"
    t.string  "facebook_url"
    t.string  "twitter_url"
    t.string  "instagram_url"
    t.string  "youtube_url"
    t.string  "mas_noticias_url"
    t.string  "tournament_name"
    t.string  "logo_url"
    t.boolean "group_table"
    t.string  "current_journey"
  end

  create_table "players", force: :cascade do |t|
    t.string  "name"
    t.string  "position"
    t.string  "logo_url"
    t.boolean "active",   default: true
    t.integer "team_id"
    t.string  "kda",      default: "0/0/0"
  end

  create_table "teams", force: :cascade do |t|
    t.string  "name"
    t.string  "logo_url"
    t.text    "description"
    t.integer "victories",        default: 0
    t.integer "defeats",          default: 0
    t.integer "kills",            default: 0
    t.integer "deaths",           default: 0
    t.integer "points",           default: 0
    t.string  "change_place_img", default: "none"
    t.integer "group_id"
    t.string  "short_name"
  end

  create_table "texts", force: :cascade do |t|
    t.string "copa_header"
    t.text   "global_rules"
    t.text   "local_rules"
    t.string "cim"
    t.string "cdl"
  end

  add_foreign_key "matches", "journeys"
end
