Group.create!([
                  {name: 'A'},
                  {name: 'B'},
                  {name: 'C'},
                  {name: 'D'}
              ])

Team.create!([
                 {name: "Lyon Gaming", logo_url: "http://pachucodev.com/lol/assets/calendario/equipos_big/equipo1.png", description: "A pesar de mantenerse invicto en la región durante los pasados 3 años, el león mexicano no ha podido dar el siguiente paso y destacar en la escena internacional. Recientemente, el equipo agregó a uno de los tiradores más destacados de Latinoamérica Sur, WhiteLotus, así como al mejor jungla de LAN, Oddie. En conjunto con Seiya, uno de los carrileros centrales más dominantes y experimentados de nuestra región, Lyon llevará sobre sus hombros la responsabilidad de no decepcionar a su afición con esta nueva alineación de jugadores estrella.", victories: 18, defeats: 2, kills: 338, deaths: 156, points: 18, change_place_img: "none", group_id:1},
                 {name: "Just Toys Havoks", logo_url: "http://pachucodev.com/lol/assets/calendario/equipos_big/equipo2.png", description: "Subcampeones de Latinoamérica Norte, el gran desempeño de Just Toys Havoks les ha merecido la admiración de muchos a pesar de su corta experiencia en Copa Latinoamérica. En esta, su tercera participación, la escuadra del caos ha generado gran expectativa con la incorporación reciente de Minibestia y EvanRL, provenientes de Robert Morris University (RMU). Sin embargo, el máximo compromiso es el que se han trazado ellos mismos: alzar la copa en la Arena Ciudad de México el próximo 20 de agosto.", victories: 13, defeats: 8, kills: 320, deaths: 235, points: 13, change_place_img: "none", group_id:2},
                 {name: "Galactic Gamers", logo_url: "http://pachucodev.com/lol/assets/calendario/equipos_big/equipo3.png", description: "Tras una temporada llena de altibajos, era tiempo de renovarse: en este torneo se incorporan Adriá “Jin” Salabert, destacado carrilero superior de la División de Honor (liga semiprofesional española); y Federico “Fr3deric” Lizondo, junglero proveniente del equipo europeo de Giants. Estos integrantes foráneos se suman a la ya consolidada línea inferior y a su nuevo carrilero central, el novato Francisco “Leza” Barragán, que con sus hábiles mecánicas no tardará en adaptarse a su equipo, aunque tendrán que arroparlo para aprovechar su gran potencial.", victories: 13, defeats: 8, kills: 234, deaths: 267, points: 13, change_place_img: "none", group_id:3},
                 {name: "Revenge eSports", logo_url: "http://pachucodev.com/lol/assets/calendario/equipos_big/equipo4.png", description: "Revenge siempre ha sido el equipo que rompe expectativas, pues a primera vista, su alineación parecería no contar con figuras que sobresalgan individualmente. Es su persistencia y trabajo en equipo lo que ha logrado sorprender a los fanáticos cada temporada. Para este torneo, Revenge abandona su tradición de tener una escuadra exclusivamente peruana y agregan a sus filas al mexicano NeeDlesS, exjugador del recientemente relegado Dash9 Gaming, y al chileno Quaker, quien jugó durante el Torneo de Apertura de la Copa LAS para Rebirth eSports.", victories: 3, defeats: 16, kills: 190, deaths: 297, points: 3, change_place_img: "none", group_id:2},
                 {name: "Zaga Talent Gaming", logo_url: "http://pachucodev.com/lol/assets/calendario/equipos_big/equipo5.png", description: "Escuadra que recientemente consiguió su ascenso a la Copa de Clausura, Zaga Talent se caracteriza por su juventud y capacidad de adaptación. Por mucho el equipo más consistente a lo largo del Circuito de Leyendas, sin duda no dejarán pasar la oportunidad de probar sus capacidades en la escena profesional. También aprovechará al máximo la valiosa incorporación de GodDragon (antes conocido como FastDragon) proveniente de la escuadra europea de Giants. Con su experiencia, personalidad y liderazgo, será una sólida guía para este equipo con hambre de triunfo.", victories: 9, defeats: 11, kills: 291, deaths: 254, points: 9, change_place_img: "none", group_id:2},
                 {name: "Brawl eSports", logo_url: "http://pachucodev.com/lol/assets/calendario/equipos_big/equipo6.png", description: "Luego de un año de haber sido relegado, BrawL regresa al circuito profesional con una alineación totalmente renovada y una nueva mascota, pues sustituyen al tucán que los acompañó por 2 años por un elefante que simboliza la fuerza y el compromiso con la victoria. Entre sus miembros destacados se encuentra Rapid, primer jugador de la isla de Curazao en llegar a ser profesional de League of Legends, así como Acerola, jugador que demostró tener un gran dominio de la jungla durante el Circuito de Leyendas y quien sin duda será pieza clave para el equipo paquidermo.", victories: 4, defeats: 15, kills: 182, deaths: 350, points: 4, change_place_img: "none", group_id:2},
             ])


Article.create!([
                    {url: "https://youtu.be/iTqWhCw4U9k", is_youtube: false, thumb_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/july_2016/DetrasDe_CopaLAN_Vid_Thumbnail_2.jpg", title: "Detrás de... la escena competitiva", subtitle: "Conoce a los talentos detrás de la CLN", date: "2016-07-08"},
                    {url: "http://lan.leagueoflegends.com/es/news/esports/esports-event/venta-de-boletos-de-la-final-de-cln-enterate", is_youtube: false, thumb_url: "http://lan.leagueoflegends.com/sites/default/files/styles/scale_xlarge/public/upload/1920x1080_tickets.jpg?itok=6iVXvvpi", title: "¡No te quedes sin boleto!", subtitle: "¡No te quedes sin boleto!", date: "2016-07-08"},
                    {url: "http://lan.leagueoflegends.com/es/news/esports/esports-editorial/obten-boletos-para-el-campeonato-mundial-2016", is_youtube: false, thumb_url: "http://lan.leagueoflegends.com/sites/default/files/styles/scale_xlarge/public/upload/worlds_ticket_schedule_header_latam.jpg?itok=Ee1R21TR", title: "Asegura tu boleto al Mundial", subtitle: "Conoce las fechas y sedes.", date: "2016-07-13"}
                ])

Journey.create!([
                    {name: "Día 1", need_brackets: false, brackets_url: nil},
                    {name: "2", need_brackets: false, brackets_url: nil},
                    {name: "3", need_brackets: false, brackets_url: nil},
                    {name: "4", need_brackets: false, brackets_url: nil},
                    {name: "5", need_brackets: false, brackets_url: nil},
                    {name: "6", need_brackets: false, brackets_url: nil},
                    {name: "7", need_brackets: false, brackets_url: nil},
                    {name: "8", need_brackets: false, brackets_url: nil},
                    {name: "Eliminatorias", need_brackets: true, brackets_url: "http://pachucodev.com/lol/img/home/brackets-team.png"},
                    {name: "Final", need_brackets: true, brackets_url: "http://pachucodev.com/lol/img/home/brackets-team.png"}
                ])

Match.create!([
  {team_a_id: 1, team_b_id: 2, score_a: 0, score_b: 0, time: "2016-07-11 21:00:00", journey_id: 1},
  {team_a_id: 1, team_b_id: 6, score_a: 0, score_b: 0, time: "2016-07-12 21:00:00", journey_id: 1},
  {team_a_id: 2, team_b_id: 4, score_a: 0, score_b: 0, time: "2016-07-13 21:00:00", journey_id: 2},
  {team_a_id: 4, team_b_id: 5, score_a: 0, score_b: 0, time: "2016-07-14 21:00:00", journey_id: 2},
  {team_a_id: 2, team_b_id: 6, score_a: 0, score_b: 0, time: "2016-06-15 13:00:00", journey_id: 3},
  {team_a_id: 5, team_b_id: 1, score_a: 0, score_b: 0, time: "2016-06-16 23:00:00", journey_id: 3},
  {team_a_id: 3, team_b_id: 2, score_a: 0, score_b: 0, time: "2016-06-17 11:00:00", journey_id: 4}
])

Match.create!([
                  {team_a_id: 1, team_b_id: 2, score_a: 2, score_b: 0, time: "2016-05-30 19:00:00", journey_id: 1},
                  {team_a_id: 5, team_b_id: 6, score_a: 0, score_b: 2, time: "2016-05-30 21:00:00", journey_id: 1},
                  {team_a_id: 4, team_b_id: 3, score_a: 0, score_b: 2, time: "2016-05-31 19:00:00", journey_id: 1},
                  {team_a_id: 1, team_b_id: 6, score_a: 2, score_b: 0, time: "2016-05-31 21:00:00", journey_id: 1},
                  {team_a_id: 3, team_b_id: 5, score_a: 2, score_b: 1, time: "2016-06-06 19:00:00", journey_id: 2},
                  {team_a_id: 2, team_b_id: 4, score_a: 2, score_b: 0, time: "2016-06-06 21:00:00", journey_id: 2},
                  {team_a_id: 3, team_b_id: 1, score_a: 1, score_b: 2, time: "2016-06-07 19:00:00", journey_id: 2},
                  {team_a_id: 4, team_b_id: 5, score_a: 0, score_b: 2, time: "2016-06-07 21:00:00", journey_id: 2},
                  {team_a_id: 2, team_b_id: 6, score_a: 2, score_b: 0, time: "2016-06-13 19:00:00", journey_id: 3},
                  {team_a_id: 5, team_b_id: 1, score_a: 0, score_b: 2, time: "2016-06-13 21:00:00", journey_id: 3},
                  {team_a_id: 6, team_b_id: 4, score_a: 2, score_b: 1, time: "2016-06-14 19:00:00", journey_id: 3},
                  {team_a_id: 3, team_b_id: 2, score_a: 0, score_b: 2, time: "2016-06-14 21:00:00", journey_id: 3},
                  {team_a_id: 2, team_b_id: 5, score_a: 1, score_b: 2, time: "2016-06-20 19:00:00", journey_id: 4},
                  {team_a_id: 6, team_b_id: 3, score_a: 0, score_b: 2, time: "2016-06-20 21:00:00", journey_id: 4},
                  {team_a_id: 1, team_b_id: 4, score_a: 2, score_b: 0, time: "2016-06-21 19:00:00", journey_id: 4},
                  {team_a_id: 2, team_b_id: 1, score_a: 1, score_b: 2, time: "2016-06-27 19:00:00", journey_id: 5},
                  {team_a_id: 5, team_b_id: 6, score_a: 2, score_b: 0, time: "2016-06-27 21:00:00", journey_id: 5},
                  {team_a_id: 3, team_b_id: 4, score_a: 2, score_b: 0, time: "2016-06-28 19:00:00", journey_id: 5},
                  {team_a_id: 6, team_b_id: 1, score_a: 0, score_b: 2, time: "2016-06-28 21:00:00", journey_id: 5},
                  {team_a_id: 5, team_b_id: 3, score_a: 0, score_b: 2, time: "2016-07-04 19:00:00", journey_id: 6},
                  {team_a_id: 4, team_b_id: 2, score_a: 0, score_b: 2, time: "2016-07-04 21:00:00", journey_id: 6},
                  {team_a_id: 1, team_b_id: 3, score_a: 2, score_b: 0, time: "2016-07-05 19:00:00", journey_id: 6},
                  {team_a_id: 5, team_b_id: 4, score_a: 2, score_b: 0, time: "2016-07-05 21:00:00", journey_id: 6},
                  {team_a_id: 6, team_b_id: 2, score_a: 0, score_b: 2, time: "2016-07-11 19:00:00", journey_id: 7},
                  {team_a_id: 1, team_b_id: 5, score_a: 2, score_b: 0, time: "2016-07-11 21:00:00", journey_id: 7},
                  {team_a_id: 4, team_b_id: 6, score_a: 2, score_b: 0, time: "2016-07-12 19:00:00", journey_id: 7},
                  {team_a_id: 2, team_b_id: 3, score_a: 1, score_b: 2, time: "2016-07-12 21:00:00", journey_id: 7},
                  {team_a_id: 5, team_b_id: 2, score_a: 0, score_b: 0, time: "2016-07-18 19:00:00", journey_id: 8},
                  {team_a_id: 3, team_b_id: 6, score_a: 0, score_b: 0, time: "2016-07-18 21:00:00", journey_id: 8},
                  {team_a_id: 4, team_b_id: 1, score_a: 0, score_b: 0, time: "2016-07-19 19:00:00", journey_id: 8}
              ])

Option.create!([
  {username: "NewUser",
   password_digest: "$2a$10$Rt2lmGAJDVLeSgvjLNItC.8CIhbYE89uFtZUzxXtU7etLHwjquUb6",
   playing_live_video: true,
   live_video_url: "https://www.youtube.com/embed/GZnfBw7t28A",
   facebook_url: "http://facebook.com/lolesportsla",
   twitter_url: "http://twitter.com/lolesportsla",
   instagram_url: "http://instagram.com/lolesportsla",
   youtube_url: "http://youtube.com/lolesportsla",
   mas_noticias_url: "http://lan.leagueoflegends.com/es/tag/esports",
   tournament_name:'El mundial',
   logo_url:'http://lan.lolesports.com/img/home/logo.png',
   group_table: false,
   current_journey: "Eliminatorias"
  }
])

Player.create!([
  {name: "Jirall", position: "Superior", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Lyon_Top_Jirall.jpg", active: true, team_id: 1, kda: "1/2/3"},
  {name: "Oddie", position: "Jungla", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Lyon_Jg_Oddie.jpg", active: true, team_id: 1, kda: "0/3/30"},
  {name: "Seiya", position: "Cental", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Lyon_Mid_Seeiya.jpg", active: true, team_id: 1, kda: "6/2/3"},
  {name: "Whitelotus", position: "Tirador", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Lyon_ADC_Whitelotus.jpg", active: true, team_id: 1, kda: "7/3/9"},
  {name: "Arce", position: "Soporte", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Lyon_Sup_Arce.jpg", active: true, team_id: 1, kda: "5/6/3"},
  {name: "Porky", position: "Superior", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Havoks_Top_Porky.jpg", active: true, team_id: 2, kda: "5/2/8"},
  {name: "LiquidDiego", position: "Jungla", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Havoks_Jg_Liquiddiego.jpg", active: true, team_id: 2, kda: "0/0/0"},
  {name: "Dcstar", position: "Central", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Havoks_Mid_Dcstar.jpg", active: true, team_id: 2, kda: "1/2/3"},
  {name: "Evanrl", position: "Tirador", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Havoks_ADC_Evanrl.jpg", active: true, team_id: 2, kda: "4/6/9"},
  {name: "Minibestia", position: "Soporte", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Havoks_Sup_Minibestia.jpg", active: true, team_id: 2, kda: "0/0/0"},
  {name: "Jin", position: "Superior", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/GG_Top_Jin.jpg", active: true, team_id: 3, kda: "1/2/3"},
  {name: "Fr3deric", position: "Jungla", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/GG_Jg_Frederic.jpg", active: true, team_id: 3, kda: "1/2/3"},
  {name: "Leza", position: "Central", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/GG_Mid_Leza.jpg", active: true, team_id: 3, kda: "1/2/3"},
  {name: "1an", position: "Tirador", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/GG_ADC_Ian.jpg", active: true, team_id: 3, kda: "1/2/3"},
  {name: "Nerzhul", position: "Soporte", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/GG_Sup_Nerzhul.jpg", active: true, team_id: 3, kda: "1/2/3"},
  {name: "Guishe", position: "Superior", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Revenge_Top_Guishe.jpg", active: true, team_id: 4, kda: "1/2/3"},
  {name: "Quakerr", position: "Jungla", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Revenge_Jg_Quaker.jpg", active: true, team_id: 4, kda: "1/2/3"},
  {name: "Rhom Wick", position: "Central", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Revenge_Mid_Rhom.jpg", active: true, team_id: 4, kda: "1/2/3"},
  {name: "Iruga", position: "Tirador", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Revenge_ADC_Iruga.jpg", active: true, team_id: 4, kda: "0/0/0"},
  {name: "Needless", position: "Soporte", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Revenge_Sup_Needless.jpg", active: true, team_id: 4, kda: "1/2/3"},
  {name: "Voltigore", position: "Superior", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Zaga_Top_Voltigore.jpg", active: true, team_id: 5, kda: "1/2/3"},
  {name: "Solidsnake", position: "Jungla", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Zaga_Jg_Solidsnake.jpg", active: true, team_id: 5, kda: "1/2/3"},
  {name: "Master Jos", position: "Central", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Zaga_Mid_Masterjos.jpg", active: true, team_id: 5, kda: "1/2/3"},
  {name: "Virusfx", position: "Tirador", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Zaga_ADC_Virusfx.jpg", active: true, team_id: 5, kda: "1/2/3"},
  {name: "Godragon", position: "Soporte", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Zaga_Sup_Goddragon.jpg", active: true, team_id: 5, kda: "1/2/3"},
  {name: "Prashant", position: "Superior", logo_url: "http://dummyimage.com/100x100", active: true, team_id: 6, kda: "1/2/3"},
  {name: "Acerola", position: "Jungla", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Brawl_Jg_Acerola.jpg", active: true, team_id: 6, kda: "1/2/3"},
  {name: "Rapid", position: "Central", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Brawl_Mid_Rapid.jpg", active: true, team_id: 6, kda: "1/2/3"},
  {name: "Manu", position: "Tirador", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Brawl_ADC_Manu.jpg", active: true, team_id: 6, kda: "1/2/3"},
  {name: "Keoo", position: "Soporte", logo_url: "http://news.cdn.leagueoflegends.com.s3.amazonaws.com/public/images/articles-latam/2016/esports/LAN/copaclausura/jugadores/Brawl_Sup_Keoo.jpg", active: true, team_id: 6, kda: "1/2/3"}
])

Text.create!([
  {
      copa_header: "<p>La fase de Clausura de la Copa LAN 2016 es la siguiente\r\noportunidad que tendrán los equipos profesionales de nuestra región para\r\nconquistar el título de Campeones de Latinoamérica Norte. La temporada regular se\r\nextiende por 8 semanas de competencia todos los <b>lunes y</b> <b>martes a partir del 30\r\nde enero.</b> El formato será de todos contra todos, ida y vuelta, al mejor de\r\n3 partidas (BO3).&nbsp; La Gran Final de este torneo de Clausura se\r\nllevará a cabo en vivo en la Arena Ciudad de México el próximo 20\r\nde agosto de 2016. ¡Muy pronto compartiremos detalles de este evento para que puedas vivir la pasión en carne propia!</p>\r\n\r\n<p>En esta ocasión, las seis escuadras que han asegurado su\r\nposición para disputar la copa son: BrawL eSports, Lyon Gaming, Revenge\r\neSports, Just Toys Havoks Gaming, Zaga Talent Gaming y Galactic Gamers.&nbsp; A continuación podrás conocer los detalles de\r\ncada alineación.&nbsp;</p>",
      local_rules: Faker::Lorem.paragraph(99),
      global_rules: Faker::Lorem.paragraph(99),
      cim: "<p> <img src='http://dummyimage.com/1200x800'> La fase de Clausura de la Copa LAN 2016 es la siguiente\r\noportunidad que tendrán los equipos profesionales de nuestra región para\r\nconquistar el título de Campeones de Latinoamérica Norte. La temporada regular se\r\nextiende por 8 semanas de competencia todos los <b>lunes y</b> <b>martes a partir del 30\r\nde enero.</b> El formato será de todos contra todos, ida y vuelta, al mejor de\r\n3 partidas (BO3).&nbsp; La Gran Final de este torneo de Clausura se\r\nllevará a cabo en vivo en la Arena Ciudad de México el próximo 20\r\nde agosto de 2016. ¡Muy pronto compartiremos detalles de este evento para que puedas vivir la pasión en carne propia!</p>\r\n\r\n<p>En esta ocasión, las seis escuadras que han asegurado su\r\nposición para disputar la copa son: BrawL eSports, Lyon Gaming, Revenge\r\neSports, Just Toys Havoks Gaming, Zaga Talent Gaming y Galactic Gamers.&nbsp; A continuación podrás conocer los detalles de\r\ncada alineación.&nbsp;</p>",
      cdl: "<p>  <img src='http://dummyimage.com/1100x300'> La fase de Clausura de la Copa LAN 2016 es la siguiente\r\noportunidad que tendrán los equipos profesionales de nuestra región para\r\nconquistar el título de Campeones de Latinoamérica Norte. La temporada regular se\r\nextiende por 8 semanas de competencia todos los <b>lunes y</b> <b>martes a partir del 30\r\nde enero.</b> El formato será de todos contra todos, ida y vuelta, al mejor de\r\n3 partidas (BO3).&nbsp; La Gran Final de este torneo de Clausura se\r\nllevará a cabo en vivo en la Arena Ciudad de México el próximo 20\r\nde agosto de 2016. ¡Muy pronto compartiremos detalles de este evento para que puedas vivir la pasión en carne propia!</p>\r\n\r\n<p>En esta ocasión, las seis escuadras que han asegurado su\r\nposición para disputar la copa son: BrawL eSports, Lyon Gaming, Revenge\r\neSports, Just Toys Havoks Gaming, Zaga Talent Gaming y Galactic Gamers.&nbsp; A continuación podrás conocer los detalles de\r\ncada alineación.&nbsp;</p>",
  }
])


Menu.create!([
    {title: 'NOTICIAS', active: true},
    {title: 'LA COPA', active: true},
    {title: 'CALENDARIO', active: true},
    {title: 'OTRAS LIGAS', active: true},
    {title: 'Torneo', active: true, parent_menu_id: 2},
    {title: 'Reglamento Global', active: true, parent_menu_id: 2},
    {title: 'Reglamento Local', active: true, parent_menu_id: 2},
    {title: 'CIM', active: true, parent_menu_id: 4},
    {title: 'CDL', active: true, parent_menu_id: 4},
 ])